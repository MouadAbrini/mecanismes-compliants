
import matplotlib.pyplot as plt
import matplotlib.font_manager
import numpy as np


plt.style.use(["science", "ieee"])

fig, ax = plt.subplots()

x = []
y = []

space = np.linspace(-0.3, 2.1, 200)

for i in range(len(space)):
    for j in range(len(space)):
        x.append(space[i])
        y.append(space[j])

plt.scatter(x, y, c="black", s=0.1)
plt.savefig("scatter.png", dpi=1000)

#plt.show()
#import tikzplotlib

#tikzplotlib.save("space.tex")