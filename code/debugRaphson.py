import matplotlib.pyplot as plt
import numpy as np


def plota(xm, ym, phi, alpha1, alpha2, alpha3, theta1, theta2, theta3, ax):

    ax.lines = []
    line, = ax.plot([], [], marker='o', c='k', lw=6, ms=10)
    line2, = ax.plot([], [], marker='o', c='b', lw=6, ms=10)
    line3, = ax.plot([], [], marker='o', c='g', lw=6, ms=10)
    line4, = ax.plot([], [], marker='o', c='r', lw=6, ms=10)
    a, b, h, c = 1, 1, 2, 4
    a2, b2 = 1, 1
    a3, b3 = 1, 1
    xp = 0
    yp = 0

    xq = c
    yq = 0
    xr = c / 2
    yr = c

    xd = a * np.cos(theta1)
    yd = a * np.sin(theta1)

    xe = xq + a * np.cos(theta2)
    ye = a * np.sin(theta2)

    xf = xr + a * np.cos(theta3)
    yf = yr + a * np.sin(theta3)

    xc1 = xm + (h * np.sqrt(3) / 3) * np.cos(np.pi / 2 + phi)
    yc1 = ym + (h * np.sqrt(3) / 3) * np.sin(np.pi / 2 + phi)

    xb1 = xm + (h * np.sqrt(3) / 3) * np.cos(np.pi / 6 - phi)
    yb1 =ym - (h * np.sqrt(3) / 3) * np.sin(np.pi / 6 - phi)

    xa1 = xm - (h * np.sqrt(3) / 3) * np.cos(phi)
    ya1 = ym - (h * np.sqrt(3) / 3) * np.sin(phi)

    xc2 = xf + b * np.cos(theta3 + alpha3)
    yc2 = yf + b * np.sin(theta3 + alpha3)

    xb2 = xe + b * np.cos(theta2 + alpha2)
    yb2 = ye + b * np.sin(theta2 + alpha2)

    xa2 = xd + b * np.cos(theta1 + alpha1)
    ya2 = yd + b * np.sin(theta1 + alpha1)




    thisx = [xp, xd, xa2]
    thisy = [yp, yd, ya2]
    line.set_data(thisx, thisy)

    thisx1 = [xa1, xc1, xb1, xa1]
    thisy1 = [ya1, yc1, yb1, ya1]
    line2.set_data(thisx1, thisy1)

    thisx2 = [xq, xe, xb2]
    thisy2 = [yq, ye, yb2]
    line3.set_data(thisx2, thisy2)

    thisx3 = [xr, xf, xc2]
    thisy3 = [yr, yf, yc2]
    line4.set_data(thisx3, thisy3)
