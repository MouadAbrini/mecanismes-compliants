
import matplotlib.pyplot as plt
import numpy as np
import pickle
import sympy as sp

show = False

plt.style.use(["science", "ieee"])

import warnings
warnings.filterwarnings("error")

with open("symb.txt", "rb") as f:
    x_a, h, d, a2, b2, y_a, phi, theta2, psi2, a3, b3, psi3, theta3, a1, b1, theta1 = pickle.load(f)

with open("thetas.txt", "rb") as f:
    [t1, t2, t3, t1_2, t2_2, t3_2] = pickle.load(f)

def test_theta(exp1, exp2):
    first_success = 0
    second_success = 0
    try:
        a = sp.lambdify(list(values.keys()), exp1, "numpy")(**values)
        first_success = 1
    except RuntimeWarning:
        pass
    try:
        b = sp.lambdify(list(values.keys()), exp2, "numpy")(**values)
        second_success = 1
    except RuntimeWarning:
        pass
    if first_success and second_success:
        return min(a, b)
    elif first_success:
        return a
    elif second_success:
        return b
    else:
        return "no solution"


def MGI(XA, YA, PHI):
    global values
    # %%
    values = {
        "x_a": XA,
        "h": 2,
        "d": 4,
        "a2": 1,
        "b2": 1,
        "y_a": YA,
        "phi": PHI,
        "a3": 1,
        "b3": 1,
        "a1": 1,
        "b1": 1,

    }

    theta1, theta2, theta3 = 0, 0, 0
    t_list = [theta1, theta2, theta3]

    theta1 = test_theta(t1, t1_2)
    if theta1 == "no solution":
        return "no solution"

    theta2 = test_theta(t2, t2_2)
    if theta2 == "no solution":
        return "no solution"

    theta3 = test_theta(t3, t3_2)
    if theta3 == "no solution":
        return "no solution"

    t_list = [theta1, theta2, theta3]

    """
    a = t1.evalf(subs=values)
    b = t1_2.evalf(subs=values)
    if not a.is_real or (not b.is_real):
        return "no solution"

    c = t2.evalf(subs=values)
    d_1 = t2_2.evalf(subs=values)

    e = t3.evalf(subs=values)
    f = t3_2.evalf(subs=values)

    l = [(a, b), (c, d_1), (e, f)]
    print(l)
    # print(l)

    for i, t in enumerate(l):
        if t[0].is_real and t[1].is_real:
            t_list[i] = min(t[0], t[1])
        elif t[0].is_real and (not t[1].is_real):
            t_list[i] = t[0]
        elif t[1].is_real and (not t[0].is_real):
            t_list[i] = t[1]
        else:
            return "no solution"
    """
    return t_list

fig, ax = plt.subplots()
ax.set_xlim([-0.3, 4.2])
ax.set_ylim([-1.2, 4])
ax.set_aspect('equal')
ax.set_title("MGI")
#ax.grid(True)
XA, YA, PHI = 1, 0, 0
a = MGI(XA, YA, PHI)

print("thetas: ", a)

theta1, theta2, theta3 = float(a[0]), float(a[1]), float(a[2])
xa, ya = XA, YA
line, = ax.plot([], [], marker='o', c='k', lw=2, ms=5)
line2, = ax.plot([], [], marker='o', c='b', lw=2, ms=5)
line3, = ax.plot([], [], marker='o', c='g', lw=2, ms=5)
line4, = ax.plot([], [], marker='o', c='r', lw=2, ms=5)
a1, b1, h, c = 1, 1, 2, 4
a2, b2 = 1, 1
a3, b3 = 1, 1
xp = 0
yp = 0

xd = a1 * np.cos(theta1)
yd = a1 * np.sin(theta1)

xb = xa + h * np.cos(PHI)
yb = ya + h * np.sin(PHI)

xc = xa + h * np.cos(np.pi / 3 + PHI)
yc = ya + h * np.sin(np.pi / 3 + PHI)

xq = xp + c
yq = 0

xe = c + a2 * np.cos(theta2)
ye = a2 * np.sin(theta2)

xf = a3 * np.cos(theta3)
yf = a3 * np.sin(theta3)

xr = c * np.cos(np.pi / 3)
yr = c * np.sin(np.pi / 3)

xf = c * np.cos(np.pi / 3) + a3 * np.cos(theta3)
yf = c * np.sin(np.pi / 3) + a3 * np.sin(theta3)

alpha1 = np.arctan2((ya - a1 * np.sin(theta1)), (xa - a1 * np.cos(theta1))) - theta1
alpha2 = np.arctan2((yb - a2 * np.sin(theta2)), (xb - c - a2 * np.cos(theta2))) - theta2
alpha3 = np.arctan2((yc - (np.sqrt(3) / 2) * c - a3 * np.sin(theta3)), (xc - c / 2 - a3 * np.cos(theta3))) - theta3
print("passives: ", [alpha1, alpha2, alpha3])

thisx = [xp, xd, xa]
thisy = [yp, yd, ya]
line.set_data(thisx, thisy)

thisx1 = [xa, xb, xc, xa]
thisy1 = [ya, yb, yc, ya]
line2.set_data(thisx1, thisy1)

thisx2 = [xq, xe, xb]
thisy2 = [yq, ye, yb]
line3.set_data(thisx2, thisy2)

thisx3 = [xr, xf, xc]
thisy3 = [yr, yf, yc]
line4.set_data(thisx3, thisy3)

if show:
    plt.show()
else:
    import tikzplotlib

    tikzplotlib.save("test.tex")
