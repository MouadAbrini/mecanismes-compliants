import matplotlib.pyplot as plt
import numpy as np

#plt.style.use("ggplot")

t = np.arange(0.0, 2.0, 0.01)
s = np.sin(2 * np.pi * t)
s2 = np.cos(2 * np.pi * t)
plt.plot(t, s, 'r', label='sin')
plt.plot(t, s2, 'b', label='cos')
plt.xlabel("time (s)")
plt.ylabel("Voltage (mV)")
plt.title("Simple plot $\\frac{\\alpha}{2}$")
plt.legend()
plt.grid(True)

import tikzplotlib

tikzplotlib.save("test.tex")