import numpy as np
import matplotlib.pyplot as plt
import os

plt.style.use(["science", "ieee"])
x = []
y = []

x1 = []
y1 = []
SR = 200


space = np.linspace(-0.3, 2.1, SR)


m = np.zeros((SR, SR))
for i in range(len(space)):
    for j in range(len(space)):
        file = str(i) + '_' + str(j)
        try:
            with open("test/"+file, "r") as f:
                if int(f.read()):
                    x.append(space[i])
                    y.append(space[j])
                else:
                    x1.append(space[i])
                    y1.append(space[j])
                #m[i, j] = int(f.read())
        except:

            pass
#print(m)
print(y)

plt.scatter(x, y, c='lightsteelblue')
plt.scatter(x1, y1, c='grey')

plt.savefig("workspace.png", dpi=1000)
